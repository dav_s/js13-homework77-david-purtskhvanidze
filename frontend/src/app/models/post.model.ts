export class Post {
  constructor(
    public id: string,
    public author: string,
    public message: string,
    public image: string,
  ) {}
}

export interface PostData {
  author: string;
  message: string;
  image: File | null;
}
