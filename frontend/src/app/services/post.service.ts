import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post, PostData } from '../models/post.model';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getPosts() {
    return this.http.get<Post[]>(environment.apiUrl + '/posts').pipe(
      map(response => {
        return response.map(postData => {
          return new Post(
            postData.id,
            postData.author,
            postData.message,
            postData.image,
          );
        });
      })
    )
  }

  createPost(postData: PostData) {
    const formData = new FormData();
    formData.append('author', postData.author);
    formData.append('message', postData.message);

    if (postData.image) {
      formData.append('image', postData.image);
    }

    return this.http.post(environment.apiUrl + '/posts', formData);
  }
}

