import { Component, OnInit, ViewChild } from '@angular/core';
import { PostService } from './services/post.service';
import { Post, PostData } from './models/post.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  posts: Post[] = [];

  constructor(private postsService: PostService) { }

  ngOnInit(): void {
    this.fetchUpdate();
  }

  fetchUpdate() {
    this.postsService.getPosts().subscribe(posts => {
      this.posts = posts;
    });
  }

  onSubmit() {
    const postData: PostData = this.form.value;
    this.postsService.createPost(postData).subscribe(() => {
      this.fetchUpdate();
      }
    );
  }
}
