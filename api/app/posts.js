const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const db = require('../fileDB');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', (req, res) => {
    const posts = db.getItems();
    return res.send(posts);
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.message) {
            return res.status(400).send({message: 'Message must be present in the request'});
        }
        const post = {
            author: req.body.author,
            message: req.body.message,
        }

        if (req.file) {
            post.image = req.file.filename;
        }

        await db.addItem(post);

        return res.send({message: 'Created new Post with ID=' + post.id});

    } catch (e) {
        next(e);
    }

});

module.exports = router;